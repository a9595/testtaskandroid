package com.andriikovalchuk.testtask01.repository.github.model.user

import com.google.gson.annotations.SerializedName

data class UserEntity(

        @field:SerializedName("avatar_url")
        val avatarUrl: String? = null,

        @field:SerializedName("id")
        val id: Int? = null,

        @field:SerializedName("login")
        val login: String? = null
)
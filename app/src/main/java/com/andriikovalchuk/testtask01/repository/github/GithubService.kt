package com.andriikovalchuk.testtask01.repository.github

import com.andriikovalchuk.testtask01.repository.github.model.github_repository.GithubRepositoryListResponse
import com.andriikovalchuk.testtask01.repository.github.model.user.FollowerEntity
import com.andriikovalchuk.testtask01.repository.github.model.user.UserListResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GithubService {

    @GET("search/repositories")
    fun getRepositories(@Query("q") query: String): Single<GithubRepositoryListResponse>

    @GET("search/users")
    fun getUsers(@Query("q") query: String): Single<UserListResponse>

    @GET("users/{userName}/followers")
    fun getUserFollowers(@Path("userName") userName: String): Single<List<FollowerEntity>>
}
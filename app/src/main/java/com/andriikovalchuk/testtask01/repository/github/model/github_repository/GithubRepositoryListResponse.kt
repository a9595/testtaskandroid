package com.andriikovalchuk.testtask01.repository.github.model.github_repository

import com.google.gson.annotations.SerializedName

data class GithubRepositoryListResponse(

        @field:SerializedName("total_count")
        val totalCount: Int? = null,

        @field:SerializedName("incomplete_results")
        val incompleteResults: Boolean? = null,

        @field:SerializedName("items")
        val items: List<GithubRepositoryEntity>? = null
)
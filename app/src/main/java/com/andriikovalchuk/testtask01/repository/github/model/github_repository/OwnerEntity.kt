package com.andriikovalchuk.testtask01.repository.github.model.github_repository

import com.google.gson.annotations.SerializedName

data class OwnerEntity(

        @field:SerializedName("avatar_url")
        val avatarUrl: String? = null,

        @field:SerializedName("id")
        val id: Int? = null,

        @field:SerializedName("login")
        val login: String? = null
)
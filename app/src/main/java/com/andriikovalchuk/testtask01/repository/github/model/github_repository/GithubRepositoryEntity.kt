package com.andriikovalchuk.testtask01.repository.github.model.github_repository

import com.google.gson.annotations.SerializedName

data class GithubRepositoryEntity(

        @field:SerializedName("owner")
        val owner: OwnerEntity,

        @field:SerializedName("stargazers_count")
        val stargazersCount: Int? = null,

        @field:SerializedName("html_url")
        val htmlUrl: String? = null,

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("description")
        val description: String? = null,

        @field:SerializedName("id")
        val id: Int? = null
)
package com.andriikovalchuk.testtask01.repository.github

import com.andriikovalchuk.testtask01.domain.model.github_repository.GithubRepositoryList
import com.andriikovalchuk.testtask01.domain.model.user.UserFollower
import com.andriikovalchuk.testtask01.domain.model.user.UserList
import com.andriikovalchuk.testtask01.domain.repositories.GithubRepository
import io.reactivex.Single

class GithubRestRepository(
    private val service: GithubService
) : GithubRepository {

    override fun getRepositories(query: String): Single<GithubRepositoryList> =
        service.getRepositories(query).map(::GithubRepositoryList)

    override fun getUsers(query: String): Single<UserList> =
        service.getUsers(query).map(::UserList)

    override fun getUserFollowers(userName: String): Single<List<UserFollower>> =
        service.getUserFollowers(userName).map { it.map(::UserFollower) }

}
package com.andriikovalchuk.testtask01.repository.github.model.user

import com.google.gson.annotations.SerializedName

data class FollowerEntity(
        @field:SerializedName("id")
        val id: Int? = null
)
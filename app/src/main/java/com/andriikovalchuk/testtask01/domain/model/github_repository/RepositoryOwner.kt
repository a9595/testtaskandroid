package com.andriikovalchuk.testtask01.domain.model.github_repository

import com.andriikovalchuk.testtask01.repository.github.model.github_repository.OwnerEntity

data class RepositoryOwner(
        val id: Int,
        val login: String,
        val avatarUrl: String
) {
    constructor(ownerEntity: OwnerEntity) : this(
            ownerEntity.id ?: -1,
            ownerEntity.login.orEmpty(),
            ownerEntity.avatarUrl.orEmpty()
    )
}
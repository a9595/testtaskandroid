package com.andriikovalchuk.testtask01.domain.model.user

object UserFactory {

    fun createList(count: Int): List<User> {
        return (0..count).map {
            User(
                it,
                "avatar #$it",
                "login $it"
            )
        }
    }

    fun createItem() = User(0, "avatarUrl", "login")
}
package com.andriikovalchuk.testtask01.domain.use_cases

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

abstract class UseCase<T> {

    open fun perform(): Single<T> {
        return doWork()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    open fun performWithoutRedirection(): Single<T> {
        return doWork()
                .subscribeOn(Schedulers.io())
    }

    open fun performOnTheSameThread(): Single<T> {
        return doWork()
    }

    protected abstract fun doWork(): Single<T>
}

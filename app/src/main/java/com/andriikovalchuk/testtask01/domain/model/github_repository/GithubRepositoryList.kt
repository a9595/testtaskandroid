package com.andriikovalchuk.testtask01.domain.model.github_repository

import com.andriikovalchuk.testtask01.repository.github.model.github_repository.GithubRepositoryListResponse

data class GithubRepositoryList(
        val totalCount: Int,
        val incompleteResults: Boolean,
        val items: List<GithubRepositoryItem>
) {
    constructor(githubRepositoryListResponse: GithubRepositoryListResponse) : this(
            githubRepositoryListResponse.totalCount ?: 0,
            githubRepositoryListResponse.incompleteResults ?: true,
            githubRepositoryListResponse.items.orEmpty().map(::GithubRepositoryItem)
    )
}

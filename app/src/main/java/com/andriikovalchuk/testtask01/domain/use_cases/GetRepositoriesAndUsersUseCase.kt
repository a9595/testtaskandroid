package com.andriikovalchuk.testtask01.domain.use_cases

import com.andriikovalchuk.testtask01.domain.model.github_repository.GithubRepositoryItem
import com.andriikovalchuk.testtask01.domain.model.github_repository.GithubRepositoryList
import com.andriikovalchuk.testtask01.domain.model.user.User
import com.andriikovalchuk.testtask01.domain.model.user.UserList
import com.andriikovalchuk.testtask01.domain.repositories.GithubRepository
import io.reactivex.Single
import io.reactivex.functions.BiFunction

open class GetRepositoriesAndUsersUseCase(
    private val githubRepository: GithubRepository,
    private val query: String? = null
) : UseCase<Pair<List<GithubRepositoryItem>, List<User>>>() {

    open fun query(query: String) = GetRepositoriesAndUsersUseCase(githubRepository, query)

    override fun doWork(): Single<Pair<List<GithubRepositoryItem>, List<User>>> {
        val query = checkNotNull(query) { "Repository query has not been provided" }

        val repositories = githubRepository.getRepositories(query)
        val users = githubRepository.getUsers(query)

        return Single
            .zip<GithubRepositoryList, UserList, Pair<GithubRepositoryList, UserList>>(
                repositories,
                users,
                BiFunction { reposParam, usersParam ->
                    reposParam to usersParam
                })
            .map { it.first.items to it.second.items }

    }
}
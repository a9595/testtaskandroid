package com.andriikovalchuk.testtask01.domain.model.user

import com.andriikovalchuk.testtask01.repository.github.model.user.FollowerEntity

data class UserFollower(val id: Int) {
    constructor(followerEntity: FollowerEntity) : this(followerEntity.id ?: -1)
}
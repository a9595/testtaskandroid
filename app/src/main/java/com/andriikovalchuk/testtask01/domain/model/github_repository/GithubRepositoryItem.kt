package com.andriikovalchuk.testtask01.domain.model.github_repository

import com.andriikovalchuk.testtask01.presentation.base.adapter.ViewType
import com.andriikovalchuk.testtask01.presentation.main_activity.adapter.AdapterConstants
import com.andriikovalchuk.testtask01.repository.github.model.github_repository.GithubRepositoryEntity

data class GithubRepositoryItem(
        val id: Int,
        val owner: RepositoryOwner,
        val stargazersCount: Int,
        val htmlUrl: String,
        val name: String,
        val description: String
) : ViewType {
    constructor(repositoryEntity: GithubRepositoryEntity) : this(
            repositoryEntity.id ?: -1,
            RepositoryOwner(repositoryEntity.owner),
            repositoryEntity.stargazersCount ?: 0,
            repositoryEntity.htmlUrl.orEmpty(),
            repositoryEntity.name.orEmpty(),
            repositoryEntity.description.orEmpty()
    )

    override fun getViewType() = AdapterConstants.REPOSITORIES
}
package com.andriikovalchuk.testtask01.domain.model.user

import com.andriikovalchuk.testtask01.presentation.base.adapter.ViewType
import com.andriikovalchuk.testtask01.presentation.main_activity.adapter.AdapterConstants
import com.andriikovalchuk.testtask01.repository.github.model.user.UserEntity

data class User(
    val id: Int,
    val avatarUrl: String,
    val login: String
) : ViewType {
    constructor(userEntity: UserEntity) : this(
        userEntity.id ?: -1,
        userEntity.avatarUrl.orEmpty(),
        userEntity.login.orEmpty()
    )

    override fun getViewType() = AdapterConstants.USERS
}
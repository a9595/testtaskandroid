package com.andriikovalchuk.testtask01.domain.repositories

import com.andriikovalchuk.testtask01.domain.model.github_repository.GithubRepositoryList
import com.andriikovalchuk.testtask01.domain.model.user.UserFollower
import com.andriikovalchuk.testtask01.domain.model.user.UserList
import io.reactivex.Single

interface GithubRepository {
    fun getRepositories(query: String): Single<GithubRepositoryList>

    fun getUsers(query: String): Single<UserList>

    fun getUserFollowers(userName: String): Single<List<UserFollower>>
}

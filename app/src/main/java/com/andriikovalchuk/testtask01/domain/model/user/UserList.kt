package com.andriikovalchuk.testtask01.domain.model.user

import com.andriikovalchuk.testtask01.repository.github.model.user.UserListResponse

data class UserList(
        val totalCount: Int,
        val items: List<User>
) {
    constructor(response: UserListResponse) : this(
            response.totalCount ?: 0,
            response.items.orEmpty().map(::User))
}
package com.andriikovalchuk.testtask01.domain.model.github_repository

object GithubRepositoryFactory {

    fun createList(count: Int) = (0..5).map {
        GithubRepositoryItem(
            it,
            createOwner(),
            it,
            "url #$it",
            "name #$it",
            "desc #$it"
        )
    }

    fun createItem() =
        GithubRepositoryItem(
            1,
            createOwner(),
            2,
            "url #$1",
            "name #$1",
            "desc #$1"
        )


    private fun createOwner() = RepositoryOwner(
        2,
        "login",
        "avatar"
    )
}
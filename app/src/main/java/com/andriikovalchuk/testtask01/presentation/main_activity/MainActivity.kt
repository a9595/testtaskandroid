package com.andriikovalchuk.testtask01.presentation.main_activity

import android.content.Context
import android.content.Intent
import com.andriikovalchuk.testtask01.presentation.base.MvpActivity
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : MvpActivity<MainActivityPresenter, MainActivityView>() {

    @Inject
    lateinit var mainActivityPresenter: MainActivityPresenter
    @Inject
    lateinit var mainActivityView: MainActivityView

    override val presenter: MainActivityPresenter get() = mainActivityPresenter
    override val presentedView: MainActivityView get() = mainActivityView

    override fun injectIntoGraph() {
        AndroidInjection.inject(this)
    }

    companion object {
        fun getIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }
}

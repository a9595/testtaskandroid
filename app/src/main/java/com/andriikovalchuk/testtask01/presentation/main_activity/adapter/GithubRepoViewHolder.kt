package com.andriikovalchuk.testtask01.presentation.main_activity.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import com.andriikovalchuk.testtask01.domain.model.github_repository.GithubRepositoryItem
import kotlinx.android.synthetic.main.item_github_repo.view.*

class GithubRepoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(item: GithubRepositoryItem) {
        itemView.name.text = item.name
    }
}

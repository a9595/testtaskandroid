package com.andriikovalchuk.testtask01.presentation.main_activity.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.andriikovalchuk.testtask01.R
import com.andriikovalchuk.testtask01.domain.model.github_repository.GithubRepositoryItem
import com.andriikovalchuk.testtask01.presentation.base.adapter.ViewType
import com.andriikovalchuk.testtask01.presentation.base.adapter.ViewTypeDelegateAdapter

class RepositoriesDelegateAdapter(onRepositoryClicked: (GithubRepositoryItem) -> Unit) : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup): GithubRepoViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_github_repo, parent, false)
        return GithubRepoViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
        val repository = item as? GithubRepositoryItem ?: throw IllegalArgumentException()
        (holder as GithubRepoViewHolder).bind(repository)
    }
}
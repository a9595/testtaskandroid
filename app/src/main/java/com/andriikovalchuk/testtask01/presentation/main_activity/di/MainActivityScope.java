package com.andriikovalchuk.testtask01.presentation.main_activity.di;

import javax.inject.Scope;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Scope
public @interface MainActivityScope {
}

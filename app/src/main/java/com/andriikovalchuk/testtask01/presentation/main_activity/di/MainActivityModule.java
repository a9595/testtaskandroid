package com.andriikovalchuk.testtask01.presentation.main_activity.di;

import android.content.Context;
import com.andriikovalchuk.testtask01.domain.use_cases.GetRepositoriesAndUsersUseCase;
import com.andriikovalchuk.testtask01.presentation.main_activity.MainActivityModel;
import com.andriikovalchuk.testtask01.presentation.main_activity.MainActivityPresenter;
import com.andriikovalchuk.testtask01.presentation.main_activity.MainActivityView;
import com.andriikovalchuk.testtask01.presentation.main_activity.adapter.GithubAdapter;
import com.andriikovalchuk.testtask01.presentation.navigation.ActivityNavigator;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class MainActivityModule {

    @MainActivityScope
    @Provides
    static MainActivityView providesMvpView(GithubAdapter adapter) {
        return new MainActivityView(adapter);
    }

    @MainActivityScope
    @Provides
    static MainActivityModel providesMvpModel(GetRepositoriesAndUsersUseCase getRepositoriesAndUsersUseCase) {
        return new MainActivityModel(getRepositoriesAndUsersUseCase);
    }

    @MainActivityScope
    @Provides
    static MainActivityPresenter providesMvpPresenter(
            MainActivityModel model,
            MainActivityView view,
            ActivityNavigator activityNavigator
    ) {
        return new MainActivityPresenter(model, activityNavigator, view);
    }

    @MainActivityScope
    @Provides
    static ActivityNavigator providesActivityNavigator(Context context) {
        return new ActivityNavigator(context);
    }

    @MainActivityScope
    @Provides
    static GithubAdapter bindsAdapter() {
        return new GithubAdapter();
    }
}

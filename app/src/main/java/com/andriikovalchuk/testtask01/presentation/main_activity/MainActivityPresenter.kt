package com.andriikovalchuk.testtask01.presentation.main_activity

import android.net.Uri
import android.os.Bundle
import com.andriikovalchuk.testtask01.domain.model.github_repository.GithubRepositoryItem
import com.andriikovalchuk.testtask01.domain.model.user.User
import com.andriikovalchuk.testtask01.presentation.base.Presenter
import com.andriikovalchuk.testtask01.presentation.base.register
import com.andriikovalchuk.testtask01.presentation.navigation.ActivityNavigator
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit
import javax.inject.Inject

open class MainActivityPresenter @Inject constructor(
    private val model: MainActivityModel,
    private val navigator: ActivityNavigator,
    view: MainActivityView
) : Presenter<MainActivityView>(view) {

    override fun bind(intentBundle: Bundle, savedInstanceState: Bundle, intentData: Uri?) {
        presentedView.rxSearchViewObservable
            .map { it.trim() }
            .debounce(SEARCH_DEBOUNCE_TIMEOUT, TimeUnit.MILLISECONDS)
            .filter { it.isNotBlank() }
            .distinct()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { loadData(it.toString()) },
                onError = { presentedView.displayError(it) },
                onComplete = { presentedView.collapseSearchView() }
            ).register(this)
    }

    open fun loadData(query: String) {
        presentedView.displayProgress()
        model.getRepositoriesAndUses(query)
            .subscribeBy(
                onSuccess = { presentedView.displayData(it) },
                onError = { presentedView.displayError(it) }
            ).register(this)
    }

    open fun navigateToRepositoryDetails(item: GithubRepositoryItem) {
        navigator.startRepositoryDetails(item)
    }

    open fun navigateToUserDetails(user: User) {
        navigator.startUserDetails(user)
    }

    companion object {
        const val SEARCH_DEBOUNCE_TIMEOUT = 250L
    }
}

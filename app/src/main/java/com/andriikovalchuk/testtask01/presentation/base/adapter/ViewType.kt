package com.andriikovalchuk.testtask01.presentation.base.adapter

interface ViewType {
    fun getViewType(): Int
} 
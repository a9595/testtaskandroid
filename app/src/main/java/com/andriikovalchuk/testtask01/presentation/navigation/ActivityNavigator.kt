package com.andriikovalchuk.testtask01.presentation.navigation

import android.content.Context
import com.andriikovalchuk.testtask01.domain.model.github_repository.GithubRepositoryItem
import com.andriikovalchuk.testtask01.domain.model.user.User

open class ActivityNavigator(private val context: Context) {

    open fun startRepositoryDetails(item: GithubRepositoryItem) {
//        val intent = DetailsActivity.getIntent(context, item)
//        context.startActivity(intent)
    }
    open fun startUserDetails(item: User) {
//        val intent = DetailsActivity.getIntent(context, item)
//        context.startActivity(intent)
    }

}

package com.andriikovalchuk.testtask01.presentation.main_activity.adapter

object AdapterConstants {
    val REPOSITORIES = 1
    val USERS = 2
}
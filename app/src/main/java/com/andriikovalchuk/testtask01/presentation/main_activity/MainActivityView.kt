package com.andriikovalchuk.testtask01.presentation.main_activity

import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.widget.SearchView
import android.widget.ViewAnimator
import com.andriikovalchuk.testtask01.R
import com.andriikovalchuk.testtask01.domain.model.github_repository.GithubRepositoryItem
import com.andriikovalchuk.testtask01.domain.model.user.User
import com.andriikovalchuk.testtask01.presentation.base.PresentedActivityView
import com.andriikovalchuk.testtask01.presentation.main_activity.adapter.GithubAdapter
import com.jakewharton.rxbinding3.InitialValueObservable
import com.jakewharton.rxbinding3.widget.queryTextChanges
import com.mcxiaoke.koi.ext.hideSoftKeyboard

open class MainActivityView(
    val adapter: GithubAdapter
) : PresentedActivityView<MainActivityPresenter>() {

    @LayoutRes
    override val layoutResId = R.layout.activity_main
    private val list: RecyclerView by bindView(R.id.list)
    private val searchView: SearchView by bindView(R.id.searchView)
    private val viewAnimator: ViewAnimator by bindView(R.id.viewAnimator)
    internal val rxSearchViewObservable: InitialValueObservable<CharSequence> by lazy {
        searchView.queryTextChanges()
    }

    override fun onViewsBound() {
        viewAnimator.displayedChild = CONTENT_WELCOME
        setupAdapter()
        attachAdapter()
    }

    private fun setupAdapter() {
        adapter.onRepositoryClicked = {
            presenter.navigateToRepositoryDetails(it)
        }
        adapter.onUserClicked = {
            presenter.navigateToUserDetails(it)
        }
    }

    private fun attachAdapter() {
        list.adapter = adapter
    }

    open fun displayData(data: Pair<List<GithubRepositoryItem>, List<User>>) {
        adapter.setData(data.first, data.second)
        viewAnimator.displayedChild = CONTENT_DATA
    }

    open fun displayError(throwable: Throwable) {

    }

    fun displayProgress() {
        viewAnimator.displayedChild = CONTENT_LOADING
    }

    fun collapseSearchView() {
        searchView.hideSoftKeyboard()
    }

    companion object {
        const val CONTENT_WELCOME = 0
        const val CONTENT_LOADING = 1
        const val CONTENT_DATA = 2
    }
}

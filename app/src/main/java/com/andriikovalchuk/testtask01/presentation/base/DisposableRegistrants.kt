package com.andriikovalchuk.testtask01.presentation.base

import io.reactivex.disposables.Disposable

interface DisposableRegistrants {
    fun registerDisposable(disposable: Disposable)
}

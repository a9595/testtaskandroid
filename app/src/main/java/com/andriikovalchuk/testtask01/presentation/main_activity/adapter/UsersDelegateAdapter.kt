package com.andriikovalchuk.testtask01.presentation.main_activity.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.andriikovalchuk.testtask01.R
import com.andriikovalchuk.testtask01.domain.model.user.User
import com.andriikovalchuk.testtask01.presentation.base.adapter.ViewType
import com.andriikovalchuk.testtask01.presentation.base.adapter.ViewTypeDelegateAdapter

class UsersDelegateAdapter(val onUserClicked: (User) -> Unit) : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup): UserViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_user, parent, false)

        return UserViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
        val user = item as? User ?: throw IllegalArgumentException()
        (holder as UserViewHolder).bind(user)
    }
}
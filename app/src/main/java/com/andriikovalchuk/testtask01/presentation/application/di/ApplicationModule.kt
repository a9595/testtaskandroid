package com.andriikovalchuk.testtask01.presentation.application.di

import android.content.Context
import com.andriikovalchuk.testtask01.presentation.application.TestTaskApplication
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Module(includes = [AndroidInjectionModule::class])
abstract class ApplicationModule {

    @Binds
    @Singleton
    internal abstract
    fun application(app: TestTaskApplication): Context
}
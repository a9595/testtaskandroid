package com.andriikovalchuk.testtask01.presentation.application.di

import com.andriikovalchuk.testtask01.presentation.main_activity.MainActivity
import com.andriikovalchuk.testtask01.presentation.main_activity.di.MainActivityModule
import com.andriikovalchuk.testtask01.presentation.main_activity.di.MainActivityScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildersModule {

    @MainActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun contributesMainActivityConstructor(): MainActivity
}
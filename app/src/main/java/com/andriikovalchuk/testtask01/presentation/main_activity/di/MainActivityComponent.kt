package com.andriikovalchuk.testtask01.presentation.main_activity.di

import com.andriikovalchuk.testtask01.presentation.main_activity.MainActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

@MainActivityScope
@Subcomponent
interface MainActivityComponent : AndroidInjector<MainActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<MainActivity>()
}

package com.andriikovalchuk.testtask01.presentation.main_activity

import com.andriikovalchuk.testtask01.domain.use_cases.GetRepositoriesAndUsersUseCase
import javax.inject.Inject

open class MainActivityModel @Inject constructor(
    private val getRepositoriesAndUsersUseCase: GetRepositoriesAndUsersUseCase
) {

    open fun getRepositoriesAndUses(query: String) = getRepositoriesAndUsersUseCase.query(query).perform()
}
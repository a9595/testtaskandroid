package com.andriikovalchuk.testtask01.presentation.main_activity.adapter

import android.support.v4.util.SparseArrayCompat
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.andriikovalchuk.testtask01.domain.model.github_repository.GithubRepositoryItem
import com.andriikovalchuk.testtask01.domain.model.user.User
import com.andriikovalchuk.testtask01.presentation.base.adapter.ViewType
import com.andriikovalchuk.testtask01.presentation.base.adapter.ViewTypeDelegateAdapter

open class GithubAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var data = ArrayList<ViewType>()
    private var delegateAdapter = SparseArrayCompat<ViewTypeDelegateAdapter>()

    var onUserClicked: (User) -> Unit = {}
    var onRepositoryClicked: (GithubRepositoryItem) -> Unit = {}

    init {
        delegateAdapter.put(
                AdapterConstants.REPOSITORIES,
                RepositoriesDelegateAdapter(onRepositoryClicked)
        )

        delegateAdapter.put(
                AdapterConstants.USERS,
                UsersDelegateAdapter(onUserClicked)
        )
    }

    override fun getItemCount(): Int = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return delegateAdapter[viewType]?.onCreateViewHolder(parent)
                ?: throw IllegalArgumentException("ViewHolder is null")
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        delegateAdapter[getItemViewType(position)]?.onBindViewHolder(viewHolder, data[position])
    }

    override fun getItemViewType(position: Int): Int = data[position].getViewType()

    open fun setData(repositories: List<GithubRepositoryItem>, users: List<User>) {
        data.clear()
        repositories.zip(users).forEach {
            data.add(it.first)
            data.add(it.second)
        }
        notifyDataSetChanged()
    }
}

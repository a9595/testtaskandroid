package com.andriikovalchuk.testtask01.presentation.application.di

import com.andriikovalchuk.testtask01.domain.repositories.GithubRepository
import com.andriikovalchuk.testtask01.repository.github.GithubRestRepository
import com.andriikovalchuk.testtask01.repository.github.GithubService
import dagger.Module
import dagger.Provides

import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    internal fun providesRestRepository(service: GithubService): GithubRepository {
        return GithubRestRepository(service)
    }
}

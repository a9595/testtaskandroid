package com.andriikovalchuk.testtask01.presentation.base

import android.support.annotation.DrawableRes
import android.widget.ImageView
import com.squareup.picasso.Picasso

fun ImageView.loadAndCropImageWithPlaceholder(imagePath: String, @DrawableRes placeholderResId: Int) {
    if (imagePath.isBlank()) {
        setImageResource(placeholderResId)
    } else {
        Picasso.get()
            .load(imagePath)
            .placeholder(placeholderResId)
            .error(placeholderResId)
            .centerInside()
            .fit()
            .into(this)
    }
}
package com.andriikovalchuk.testtask01.presentation.application.di

import com.andriikovalchuk.testtask01.presentation.application.TestTaskApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        BuildersModule::class,
        ApplicationModule::class,
        UseCaseModule::class,
        RepositoryModule::class,
        ServiceModule::class
    ]
)
interface TestTaskAppComponent : AndroidInjector<TestTaskApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<TestTaskApplication>()
}

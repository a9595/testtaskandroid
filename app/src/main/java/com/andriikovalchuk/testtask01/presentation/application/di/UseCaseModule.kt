package com.andriikovalchuk.testtask01.presentation.application.di

import com.andriikovalchuk.testtask01.domain.repositories.GithubRepository
import com.andriikovalchuk.testtask01.domain.use_cases.GetRepositoriesAndUsersUseCase
import dagger.Module
import dagger.Provides

@Module
internal class UseCaseModule {

    @Provides
    internal fun providesGetRepositoriesAndUsersUseCase(githubRepository: GithubRepository): GetRepositoriesAndUsersUseCase =
        GetRepositoriesAndUsersUseCase(githubRepository)
}
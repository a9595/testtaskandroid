package com.andriikovalchuk.testtask01.presentation.base

import io.reactivex.disposables.Disposable

fun Disposable.register(registrants: DisposableRegistrants) {
    registrants.registerDisposable(this)
}

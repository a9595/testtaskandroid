package com.andriikovalchuk.testtask01.presentation.base

import android.content.Context
import android.support.v7.app.ActionBar
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View

abstract class PresentedActivityView<PresenterType : Any>
    : ContextPresentedView<PresenterType, MvpActivity<*, *>>() {

    private var activity: MvpActivity<*, *>? = null

    override val context: Context
        get() = activity ?: throw IllegalStateException("This view must be bound to activity")

    override val viewFinder: PresentedView<PresenterType, *>.(Int) -> View?
        get() = { activity?.findViewById(it) }

    override fun bindUiElements(boundingView: MvpActivity<*, *>, presenter: PresenterType) {
        activity = boundingView
        boundPresenter = presenter
        onViewsBound()
    }

    open val actionBar: ActionBar?
        get() = activity?.getSupportActionBar()

    open fun setActionBar(toolbar: Toolbar) {
        activity?.setSupportActionBar(toolbar)
    }

    open fun inflateMenu(menu: Menu, menuInflater: MenuInflater) {
        /* NO OP */
    }

    open fun onOptionsItemSelected(item: MenuItem): Boolean {
        return false
    }

    open fun invalidateOptionsMenu() {
        activity?.invalidateOptionsMenu()
    }
}

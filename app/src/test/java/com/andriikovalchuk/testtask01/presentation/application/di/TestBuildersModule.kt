package com.andriikovalchuk.testtask01.presentation.application.di

import com.andriikovalchuk.testtask01.presentation.main_activity.MainActivity
import com.andriikovalchuk.testtask01.presentation.main_activity.di.MainActivityScope
import com.andriikovalchuk.testtask01.presentation.main_activity.di.TestMainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class TestBuildersModule {

    @MainActivityScope
    @ContributesAndroidInjector(modules = [TestMainActivityModule::class])
    abstract fun contributesActivityConstructor(): MainActivity
}
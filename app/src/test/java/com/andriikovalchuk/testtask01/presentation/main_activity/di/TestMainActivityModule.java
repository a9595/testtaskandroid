package com.andriikovalchuk.testtask01.presentation.main_activity.di;


import com.andriikovalchuk.testtask01.presentation.main_activity.MainActivityModel;
import com.andriikovalchuk.testtask01.presentation.main_activity.MainActivityPresenter;
import com.andriikovalchuk.testtask01.presentation.main_activity.MainActivityView;
import com.andriikovalchuk.testtask01.presentation.main_activity.adapter.GithubAdapter;
import com.andriikovalchuk.testtask01.presentation.navigation.ActivityNavigator;
import dagger.Module;
import dagger.Provides;

import static org.mockito.Mockito.mock;

@Module
public abstract class TestMainActivityModule {

    @MainActivityScope
    @Provides
    static MainActivityView providesMvpView(GithubAdapter adapter) {
        return new MainActivityView(adapter);
    }

    @MainActivityScope
    @Provides
    static MainActivityModel providesMvpModel() {
        return mock(MainActivityModel.class);
    }

    @MainActivityScope
    @Provides
    static MainActivityPresenter providesMvpPresenter() {
        return mock(MainActivityPresenter.class);
    }

    @MainActivityScope
    @Provides
    static ActivityNavigator providesActivityNavigator() {
        return mock(ActivityNavigator.class);
    }

    @MainActivityScope
    @Provides
    static GithubAdapter bindsAdapter() {
        return mock(GithubAdapter.class);
    }
}

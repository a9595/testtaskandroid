package com.andriikovalchuk.testtask01.presentation.main_activity

import com.andriikovalchuk.testtask01.domain.model.github_repository.GithubRepositoryFactory
import com.andriikovalchuk.testtask01.domain.model.github_repository.GithubRepositoryItem
import com.andriikovalchuk.testtask01.domain.model.user.User
import com.andriikovalchuk.testtask01.domain.model.user.UserFactory
import com.andriikovalchuk.testtask01.fixedCapture
import com.andriikovalchuk.testtask01.presentation.navigation.ActivityNavigator
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Captor
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.mockito.Mockito.`when` as whenDo

class MainActivityPresenterTest {

    @get:Rule
    val rule: MockitoRule = MockitoJUnit.rule()

    @Mock
    internal lateinit var model: MainActivityModel
    @Mock
    internal lateinit var navigator: ActivityNavigator
    @Mock
    internal lateinit var presentedView: MainActivityView
    @Captor
    internal lateinit var exceptionCaptor: ArgumentCaptor<Exception>
    @Captor
    internal lateinit var githubRepoCaptor: ArgumentCaptor<GithubRepositoryItem>
    @Captor
    internal lateinit var userCaptor: ArgumentCaptor<User>
    @InjectMocks
    internal lateinit var presenter: MainActivityPresenter

    @Test
    fun shouldLoadDataAndDisplay() {
        // given
        val repos = GithubRepositoryFactory.createList(5)
        val users = UserFactory.createList(5)
        val data = repos to users
        val query = "repoOrUserName"
        whenDo(model.getRepositoriesAndUses(anyString())).thenReturn(Single.just(data))

        // when
        presenter.loadData(query)

        // then
        verify(model).getRepositoriesAndUses(query)
    }

    @Test(expected = Exception::class)
    fun shouldLoadDataAndDisplayErrorIfError() {
        // given
        val query = "repoOrUserName"
        val exception = Exception("error")
        whenDo(model.getRepositoriesAndUses(anyString())).thenThrow(exception)

        // when
        presenter.loadData(query)

        // then
        verify(presentedView).displayError(exceptionCaptor.fixedCapture())
        assertEquals(exception, exceptionCaptor.value)
    }

    @Test
    fun shouldNavigateToRepositoryDetails() {
        // given
        val repositoryItem = GithubRepositoryFactory.createItem()

        // when
        presenter.navigateToRepositoryDetails(repositoryItem)

        // then
        verify(navigator).startRepositoryDetails(githubRepoCaptor.fixedCapture())
        assertEquals(repositoryItem, githubRepoCaptor.value)
    }

    @Test
    fun shouldNavigateToUserDetails() {
        // given
        val user = UserFactory.createItem()

        // when
        presenter.navigateToUserDetails(user)

        // then
        verify(navigator).startUserDetails(userCaptor.fixedCapture())
        assertEquals(user, userCaptor.value)
    }
}
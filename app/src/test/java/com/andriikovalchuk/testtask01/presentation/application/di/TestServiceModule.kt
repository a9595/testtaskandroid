package com.andriikovalchuk.testtask01.presentation.application.di

import com.andriikovalchuk.testtask01.repository.github.GithubService
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import org.mockito.Mockito.mock

@Module
class TestServiceModule {

    @Provides
    internal fun provideGson() = mock(Gson::class.java)

    @Provides
    internal fun providesService(okHttpClient: OkHttpClient, gson: Gson) =
        mock(GithubService::class.java)

    @Provides
    internal fun provideOkHttpClient(okHttpCache: Cache) =
        mock(OkHttpClient::class.java)

    @Provides
    internal fun provideOkHttpCache() = mock(Cache::class.java)
}

package com.andriikovalchuk.testtask01.presentation.application.di

import com.andriikovalchuk.testtask01.domain.repositories.GithubRepository
import com.andriikovalchuk.testtask01.domain.use_cases.GetRepositoriesAndUsersUseCase
import dagger.Module
import dagger.Provides
import org.mockito.Mockito.mock

@Module
internal class TestUseCaseModule {

    @Provides
    internal fun providesGetRepositoriesAndUsersUseCase(githubRepository: GithubRepository): GetRepositoriesAndUsersUseCase =
        mock(GetRepositoriesAndUsersUseCase::class.java)
}
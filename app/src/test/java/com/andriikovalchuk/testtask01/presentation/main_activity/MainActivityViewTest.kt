package com.andriikovalchuk.testtask01.presentation.main_activity

import android.support.v7.widget.LinearLayoutManager
import base.MvpActivityBaseTest
import com.andriikovalchuk.testtask01.domain.model.github_repository.GithubRepositoryFactory
import com.andriikovalchuk.testtask01.domain.model.github_repository.GithubRepositoryItem
import com.andriikovalchuk.testtask01.domain.model.user.User
import com.andriikovalchuk.testtask01.domain.model.user.UserFactory
import com.andriikovalchuk.testtask01.fixedCapture
import com.andriikovalchuk.testtask01.presentation.main_activity.MainActivityView.Companion.CONTENT_DATA
import com.andriikovalchuk.testtask01.presentation.main_activity.MainActivityView.Companion.CONTENT_WELCOME
import com.andriikovalchuk.testtask01.presentation.main_activity.adapter.GithubAdapter
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
open class MainActivityViewTest : MvpActivityBaseTest<
        MainActivity, MainActivityPresenter, MainActivityView>() {

    override val testActivityClass: Class<MainActivity>
        get() = MainActivity::class.java

    @get:Rule
    val rule: MockitoRule = MockitoJUnit.rule()

    @Captor
    lateinit var repositoryCaptor: ArgumentCaptor<List<GithubRepositoryItem>>
    @Captor
    lateinit var userCaptor: ArgumentCaptor<List<User>>

    @Test
    fun shouldAttachAdapterOnBound() {
        assertNotNull(testedView.list.adapter)
        assertTrue(testedView.list.adapter is GithubAdapter)
    }

    @Test
    fun shouldDisplayWelcomeContentOnBind() {
        assertEquals(CONTENT_WELCOME, testedView.viewAnimator.displayedChild)
    }

    @Test
    fun shouldSetLayoutManagerOnBound() {
        assertNotNull(testedView.list.layoutManager)
        assertTrue(testedView.list.layoutManager is LinearLayoutManager)
    }

    @Test
    fun shouldSetupRepositoryOnClickListenerOnBound() {
        testedView.adapter
    }

    @Test
    fun shouldDisplayData() {
        // given
        val repos = GithubRepositoryFactory.createList(5)
        val users = UserFactory.createList(5)
        val data = repos to users

        // when
        testedView.displayData(data)

        // then
        verify(testedView.adapter).setData(
            repositoryCaptor.fixedCapture(),
            userCaptor.fixedCapture()
        )
        assertEquals(repos, repositoryCaptor.value)
        assertEquals(users, userCaptor.value)

        assertEquals(testedView.viewAnimator.displayedChild, CONTENT_DATA)
    }
}
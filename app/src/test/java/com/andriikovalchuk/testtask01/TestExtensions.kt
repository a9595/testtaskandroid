package com.andriikovalchuk.testtask01

import org.mockito.ArgumentCaptor
import org.mockito.Mockito

inline fun <reified T : Any> javaClass(): Class<T> = T::class.java

fun <T> anyObject(): T {
    Mockito.anyObject<T>()
    return uninitialized()
}

fun <T> eqNonNullable(item: T): T {
    Mockito.eq(item)
    return uninitialized()
}

fun <T> ArgumentCaptor<T>.fixedCapture(): T {
    capture()
    return uninitialized()
}

private fun <T> uninitialized(): T = null as T

